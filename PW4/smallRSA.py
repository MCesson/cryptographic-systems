##################
### Encryption ###
##################

# dealing with the shared alphabet on Moodle
dictLetterToNumber = { # encrypt in English with numbers for Lithuanian alphabet order
    " ": 0,
    "A": 1,
    "B": 3,
    "C": 4,
    "D": 6,
    "E": 7,
    "F": 10,
    "G": 11,
    "H": 12,
    "I": 13,
    "J": 16,
    "K": 17,
    "L": 18,
    "M": 19,
    "N": 20,
    "O": 21,
    "P": 22,
    "Q": 23,
    "R": 24,
    "S": 25,
    "T": 27,
    "U": 28,
    "V": 31,
    "W": 32,
    "X": 33,
    "Y": 15,
    "Z": 34
}

def encryptRSA(plaintext, dictio, e, n):
    return " ".join([str(dictio[i]**e % n) for i in plaintext])

# text
e, n = 199, 1943 # public key of your partner
print(encryptRSA("A GREAT MESSAGE", dictLetterToNumber, e, n))

# signature
e, n = 305, 377 # your own private key
print(encryptRSA("I LOVE YOUR SIGNATURE", dictLetterToNumber, e, n))


##################
### Decryption ###
##################

# dealing with the shared alphabet on Moodle, getting directly the data from a copy/paste of Moodle on a file
listNumberToLetter = [" "] # decrypt from numbers towards Lithuanian letters
with open("data.lang","r") as file:
    for line in file:
        data = line.split("\t")
        if len(data) > 1: # not an empty line in the current file
            listNumberToLetter += data[1]

def decryptRSA(ciphertext, dictio, d, n):
    return "".join([dictio[i**d % n] for i in map(int, ciphertext.split(" "))])

# text
d, n = 305, 377 # your own private key
c1 = "12 310 317 317 333 0 301 1 340 208 301 310 0 208 0 176 333 300 0 84 333 318 228 0 301 310 103 103 1 176 310"
print(decryptRSA(c1, listNumberToLetter, d, n))

# signature
d, n = 199, 1943 # public key of your partner
sign = "1688 0 1 153 0 420 28 473 0 1556 1361 28 0 473 1 1561 0 496 761 28 1499 496 0 153 74"
print(decryptRSA(sign, listNumberToLetter, d, n))