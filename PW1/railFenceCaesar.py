### Choose your alphabet
import string
# alphabet = string.printable # combination of digits, ascii letters, punctuation and whitespace
# alphabet = string.ascii_letters # all ascii letters (upper and lowercase)
alphabet = string.ascii_uppercase # only uppercase ascii letters
# you can create your own but remember that letters' order must remain the same between encryption & decryption


### Perform the Rail Fence encryption

def encryptRF(plainText,keyRF):
    if keyRF < 2: # 1 row & 0 row = len(text) rows
        return plainText
    else:
        nbOfChar = len(plainText)
        cipherText = ""
        for i in range(keyRF):
            for j in range(i,nbOfChar,2*(keyRF-1)):
                cipherText += plainText[j]
                # A letter on a descendant slope of the zig-zag may have a corresponding ascendant
                # letter on the same row. If so, this letter is the following in the ciphertext.
                if 0 < i < keyRF-1 and j+2*(keyRF-1-i) < nbOfChar: # If not on an edge of the zig-zag and if plaintext is long enough
                    cipherText += plainText[j+2*(keyRF-1-i)]
        return cipherText


### Perform the Rail Fence decryption

def decryptRF(text,keyRF):
    if keyRF < 2: # 1 row & 0 row = len(text) rows
        return text
    else:
        nbOfChar, N = len(text), 2*(keyRF-1)
        d, m = nbOfChar//N, nbOfChar%N

        f = lambda n: 1 if n > 0 else 0
        start = d+f(m)
        upper, lower = [], []

        for size in [2*d+f(m-i) for i in range(1,keyRF-1)]: # unknot intermediate lines of the zig-zag (not the 1st nor the last)
            upper = upper + [text[start:start+size:2]]
            lower = [text[start+1:start+size:2]] + lower
            start += size

        blocks = [text[:d+f(m)]] + upper + [text[-d-f(m-keyRF+1):]] + lower

        return "".join([block[i] for i in range(d) for block in blocks] + [blocks[i][-1] for i in range(m)])


### Perform the Caesar encryption

def encryptC(text,keyC):
    nbOfChar = len(alphabet)
    cipher = dict()
    for i, chr in enumerate(alphabet):
        cipher[chr] = alphabet[(i+keyC)%nbOfChar]
    return "".join([cipher[chr] for chr in text if chr in cipher])

### Perform the Caesar decryption

def decryptC(text,keyC):
    return encryptC(text,-keyC)


### Interact with the user

def askingInputs(action):
    text = input("Insert the text to %s, please use only those characters: %s\n"%(action, alphabet)) # other characters will be deleted
    keyRF = int(input("Insert the key (number) for the Rail Fence cipher %sion:\n"%(action))) % (len(text)//2)
    keyC = int(input("Insert the key (number) for the Caesar cipher %sion:\n"%(action))) % len(alphabet)
    print("Here is the %sed text:"%(action))
    return text, keyRF, keyC

cmd = input("Insert E for (E)ncrypt or insert D for (D)ecrypt.\n").lower()[0]
# Can't wait for the "match" statement! (python3.10)

if cmd == "e":
    text, keyRF, keyC = askingInputs("encrypt")
    print(encryptRF(encryptC(text,keyC),keyRF))
elif cmd == "d":
    text, keyRF, keyC = askingInputs("decrypt")
    print(decryptRF(decryptC(text,keyC),keyRF))
elif cmd == "q":
    exit()
else:
    print("Wrong command, please retry...")
    exit()
