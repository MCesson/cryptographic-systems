# Cryptographic Systems

Sources code from cryptographic systems related personal works.  
Feel free to clone the project, hijack it, make new releases or anything else friendly with the free software spirit ;)

## Personal work 1 : transposition ciphers

### Description
Combining two elementary ciphers, **Rail Fence cipher** for transposition-like ciphers & **Caesar cipher** for substitution-like ones, to create an easy to use and pretty safe cipher for hand made transmissions!  
Please keep in mind that this combined cipher is still subject to cryptanalysis and computer made brute force attacks.
It is only interesting to study for the *relative security* provided by such an easy use.

### Script's use
```
$ python3 railFenceCaesar.py
```
and follow the instructions!  

*Good to know:* the rail fence decryption - function decryptRF(a,b) - is not perfectly working. One key out of three for key values greater than 6 introduces a slight shift in deciphering (the deciphered text is still widely understandable, though) and I don't know why yet!

## Practical work 4 : RSA with small requirement on keys' length

### Description
This script has been designed in order to fastly cipher plaintext and to decipher ciphertext during related lectures, following the RSA algorithm.

### Script's use
Edit the file writing your inputs (texts and keys) to get appropriated output.
Some inputs examples remain in the file, what might be useful to remember how to use it.
However you can keep in mind that strings are used, both for plain- and cipher- texts, with letters in plaintexts and numbers separated by spaces in ciphertexts.   

Once you're done with editing, you can run
```
$ python3 smallRSA.py
```

## License
All files are published under the GPL-3.0 license.